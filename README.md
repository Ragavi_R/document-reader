# Document Reader

**Objective:**
     The intent of the challenge is to extract the data present in the documents and
convert them into textual data

**Descrition**
The file is uploaded . 

The application should read the contents of the file and
fetch the data in textual format.

Based on the type of document, the data should be displayed .

**Used Technologies:-** 
 
  - Node JS
  - HTML 
  - CSS
  - npm-Tesseract
  

